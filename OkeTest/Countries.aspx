﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Countries.aspx.cs" Inherits="OkeTest.Countries" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="StylesCss.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="divcss">
            <asp:Panel ID="LeftPanel" runat="server" Width="307px" CssClass="LeftPanel" BackColor="#99CCFF">
                kasia

                <asp:Repeater ID="Repeater1" runat="server" DataSourceID="XmlDataSource1">
                    <ItemTemplate>
                        <asp:ImageButton ID="Button" runat="server" CommandArgument='<%#XPath("Name") %>' ImageUrl='<%#XPath("FlagURL") %>' OnClick="GetCountyInfo_Click" Width="150px" Height="100px" />
                    </ItemTemplate>

                </asp:Repeater>


            </asp:Panel>
            <asp:Panel ID="InfoPanel" runat="server" CssClass="InfoPanel" Height="65px" BackColor="#33CCFF" Width="332px">

                <div>
                    Country :&nbsp;&nbsp;
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                </div>
                <div>
                    Capitol :&nbsp;&nbsp;&nbsp;  
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                </div>
                <div>
                    Currency :
                    <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                </div>

            </asp:Panel>
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:Label ID="LbError" runat="server" Text="*All fields are required" ForeColor="#FF0066" Visible="False"></asp:Label>
            <asp:Panel ID="FormPanel" runat="server" CssClass="FormPanel" Height="143px" Width="332px" BackColor="#33CCFF">
                <div style="text-align: center">Add a new country</div>
                <div>
                    Country: 
                <asp:TextBox ID="TbCountry" runat="server" Style="margin-left: 14px" Width="183px"></asp:TextBox> 
                </div>
                <div>
                    Capitol: 
                 <asp:TextBox ID="TbCapitol" runat="server" Style="margin-left: 19px" Width="183px"></asp:TextBox>
                </div>
                <div>
                    Currency: 
                 <asp:TextBox ID="TbCurrency" runat="server" Style="margin-left: 7px" Width="183px"></asp:TextBox>
                </div>
                <div>
                    Flag URL:<asp:TextBox ID="TbFlagUrl" runat="server" Style="margin-left: 6px" Width="183px"></asp:TextBox>
                </div>
                <div style="text-align: center">
                    <asp:Button ID="BtnOk" runat="server" Text="OK" OnClick="BtnOk_Click" PostBackUrl="~/Countries.aspx" />
                </div>

            </asp:Panel>

        </div>
    </form>
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/Countries.xml" XPath="Countries/Country"></asp:XmlDataSource>
</body>

</html>
