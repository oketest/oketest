﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using XmlLib;

namespace OkeTest
{
    public partial class Countries : System.Web.UI.Page
    {
        //TODO dodac mozliwosc kasowania i modyfikacji Krajów
        //TODO Dodać wiecej detali opisujace kraje
        //TODO stworzyć podział na kontynenty
        //TODO po stworzeniu nowych detali takich jak np ludność, mozna by zrobić sortowanie wg tej wartosci
        //TODO Tworzenie Error Logów
        //TODO Dodac Unit Test


        private XmlTransaction Xml;
        private List<Country> CountryList = new List<Country>();


        protected void Page_Load(object sender, EventArgs e)
        {
            string dir = Server.MapPath(@"~\Countries.xml");
            Xml = new XmlTransaction(dir);
            CountryList = Xml.Deserialize();

        }

        protected void GetCountyInfo_Click(object sender, ImageClickEventArgs e)
        {

            ImageButton button = sender as ImageButton;

            Country country = CountryList.Find(x => x.Name == button.CommandArgument);

            Label1.Text = country.Name;
            Label2.Text = country.Capitol;
            Label3.Text = country.Currency;

        }

        protected void BtnOk_Click(object sender, EventArgs e)
        {
            try
            {
                Xml.AddNewCountry(CountryList, TbCountry.Text, TbCapitol.Text, TbCurrency.Text, TbFlagUrl.Text);
                Xml.Serialize(CountryList);

                Response.Redirect("Countries.aspx");
            }
            catch (Exception)
            {

                LbError.Visible = true;
            }

        }
    }
}