﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace XmlLib
{
   public interface IXmlTransaction
    {
        void AddNewCountry(List<Country> List, string Name, string Capitol, string Currency, string FlagUrl);
        List<Country> Deserialize();
        void Serialize(List<Country> List);
    }
}
