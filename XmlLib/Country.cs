﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace XmlLib
{

    //[Serializable, XmlRoot("Countries")]
    public class Country
    {

        public string Name { get; set; }
        public string Capitol { get; set; }
        public string Currency { get; set; }
        public string FlagURL { get; set; }

        public Country()
        {

        }

        public Country(string name, string capitol, string currency, string flagUrl)
        {
            Name = name;
            Capitol = capitol;
            Currency = currency;
            FlagURL = flagUrl;
        }
    }
}
