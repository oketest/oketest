﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XmlLib
{


    public class XmlTransaction : IXmlTransaction
    {

        private string _DirPath;

        public XmlTransaction(string dir)
        {
            _DirPath = dir;

        }


        public void AddNewCountry(List<Country> List, string Name, string Capitol, string Currency, string FlagUrl)
        {
            if (Name == "" || Capitol == "" || Currency == "" || FlagUrl == "")
            {
                throw new Exception("Text Box can't be null");
            }
            else
            {
                List.Add(new Country(Name, Capitol, Currency, FlagUrl));
            }
        }

        public List<Country> Deserialize()
        {
            List<Country> Countries = new List<Country>();
            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "Countries";
            xRoot.IsNullable = true;

            XmlSerializer serializer = new XmlSerializer(typeof(List<Country>), xRoot);
            StreamReader reader = new StreamReader(_DirPath);

            Countries = (List<Country>)serializer.Deserialize(reader);
            reader.Dispose();
            return Countries;
        }

        public void Serialize(List<Country> List)
        {

            XmlRootAttribute xRoot = new XmlRootAttribute();
            xRoot.ElementName = "Countries";
            xRoot.IsNullable = true;
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");
            XmlSerializer Serializer = new XmlSerializer(typeof(List<Country>), xRoot);
            StreamWriter writer = new StreamWriter(_DirPath);

            Serializer.Serialize(writer, List,ns);
            writer.Dispose();
        }
    }
}